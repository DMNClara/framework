# Framework de Clara De Moliner

## Liste des fonctionnalités
Ce framework comprend la mise en forme :

* D’alertes ( `_alertes.scss` )
* D’un fil d’ariane ( `_ariane.scss` )
* De boutons ( `_boutons.scss` )
* D’un header ( `_hearder.scss` )
* D’un jumbotron ( `_jumbotron.scss` )
* D’un menu ( `_navigation.scss` )
* D’une pagination ( `_pagination.scss` )
* Des différentes règles typographiques ( `_typo.scss` )

D’autres fichiers sont présents :

* Les variables utilisées dans les différents fichiers scss ( `_var-framework.scss` )
* *Main* regroupant tous les .scss ( `main.scss` )

Ce framework est responsive.

## Utilisation
Pour conserver un HTML sémantique , il est nécessaire, dans le(s) futur(s) fichier(s) style, d’*_extends* les classes du framework dans les classes définies par l’intégrateur.

## Rendus
### sur arche :
* ce fichier

### sur bitbucket :
* le code source [https://bitbucket.org/DMNClara/framework](https://bitbucket.org/DMNClara/framework)

### sur webetu :
* rendu visible :
[https://webetu.iutnc.univ-lorraine.fr/www/demoline1u/FRAMEWORK/framework.html](https://webetu.iutnc.univ-lorraine.fr/www/demoline1u/FRAMEWORK/framework.html)